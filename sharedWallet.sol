pragma solidity >=0.7.0 <0.9.0;
    
import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/utils/math/SafeMath.sol";

contract wallet {

    using SafeMath for uint64;


    address owner;
    uint64 contractBalance;

    constructor() public {
        owner = msg.sender;
        
    }

    struct Payment {
        uint64 amount;
        uint timestamps;
    }

    struct Balance {
        uint64 totalBalance;
        uint numPayments;
        mapping(uint => Payment) payments;
    }

    mapping(address => Balance) public balance;

    function getBalance() public view returns(uint64) {
        return contractBalance;
    }

    function sendToContract() public payable {
        contractBalance.add(uint64(msg.value));
        balance[msg.sender].totalBalance.add(uint64(msg.value));
        Payment memory payment = Payment(uint64(msg.value), block.timestamp);
        balance[msg.sender].payments[balance[msg.sender].numPayments] = payment;
        balance[msg.sender].numPayments++;
    }

    receive() external payable {
        sendToContract();
    }

    function ownerWithdrawAll(address payable _to) public {
        require(msg.sender == owner, "Not owner of contract.");
        contractBalance = 0;
        _to.transfer(contractBalance);
    }

    function allocateUserBalance(address _to, uint64 _amount) public {
        require(msg.sender == owner, "Not owner of the contract.");
        balance[_to].totalBalance = _amount;
    }

    function removeUserBalance(address _to, uint64 _amount) public view {
        require(msg.sender == owner, "Not owner of the contract.");
        balance[_to].totalBalance.sub(_amount);
    }

    function userWithdraw(address payable _to, uint64 _amount) public {
        require(balance[msg.sender].totalBalance >= _amount, "Not enough funds.");
        balance[msg.sender].totalBalance.sub(_amount);
        _to.transfer(_amount);
    }


}
